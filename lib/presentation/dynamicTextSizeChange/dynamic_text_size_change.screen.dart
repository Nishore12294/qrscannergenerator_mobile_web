import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'controllers/dynamic_text_size_change.controller.dart';

class DynamicTextSizeChangeScreen
    extends GetView<DynamicTextSizeChangeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DynamicTextSizeChangeScreen'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'DynamicTextSizeChangeScreen is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
