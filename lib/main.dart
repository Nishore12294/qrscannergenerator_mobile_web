import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import 'package:get/get.dart';

import 'app/modules/dynamicTextSizeChanges/views/RestartWidget.dart';
import 'app/routes/app_pages.dart';

void main() {
  runApp(
    // RestartWidget(
    //   child:  GetMaterialApp(
    //     title: "Application",
    //     initialRoute: AppPages.INITIAL,
    //     getPages: AppPages.routes,
    //   ),
    // ),
    Phoenix(
      child:GetMaterialApp(
        title: "Application",
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
      ),
    ),
    // GetMaterialApp(
    //     title: "Application",
    //     initialRoute: AppPages.INITIAL,
    //     getPages: AppPages.routes,
    //   ),
  );
}
