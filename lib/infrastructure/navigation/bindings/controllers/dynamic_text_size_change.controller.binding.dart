import 'package:get/get.dart';

import '../../../../presentation/dynamicTextSizeChange/controllers/dynamic_text_size_change.controller.dart';

class DynamicTextSizeChangeControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DynamicTextSizeChangeController>(
      () => DynamicTextSizeChangeController(),
    );
  }
}
