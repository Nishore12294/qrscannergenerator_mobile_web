/*
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_core/src/get_main.dart';

import '../controllers/qr_scanner_controller.dart';
import 'package:jsqr/scanner.dart';
import 'dart:html' as html;
import 'dart:ui' as ui;

class QrScannerView extends GetView<QrScannerController> {

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("QR Web scanner"),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<bool>(
              future: controller.camAvailableF,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text("ERROR: ${snapshot.error}");
                }
                if (snapshot.hasData) {
                  if (snapshot.data != null) {
                    return (Text("Camera is available"));
                  }
                  return (Text("No camera available"));
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),

            SizedBox(height: 10),
            RaisedButton(
              child: Text("Scan QR Code"),
              onPressed:() {
                _openScan(context);
              },
            ),
            SizedBox(height: 10),
          GetBuilder<QrScannerController>(
            id: 'qrWebScannerId',
            init: QrScannerController(),
            builder: (value) => Text(
                controller.code.value != "" ?controller.code.value : "" ,
              )),
          ],
        ),
      ),

    );
  }

  void _openScan(BuildContext context) async {
    var code = await showDialog(
        context: context,
        builder: (BuildContext context) {
          // var height = MediaQuery.of(context).size.height;
          // var width = MediaQuery.of(context).size.width;
          return AlertDialog(
            insetPadding: EdgeInsets.all(5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            title: const Text('Scan QR Code'),
            content: Container(
              // height: height - 20,
                width: 640,
                height: 480,
                child: Scanner()),
          );
        });

    print("My Scanned Value /CODE::: $code");

    controller.code.value = code;
    Get.find<QrScannerController>().scannedValue();
   // print("My Scanned controller Value /CODE: "+controller.code.toString());
  }
}
*/
