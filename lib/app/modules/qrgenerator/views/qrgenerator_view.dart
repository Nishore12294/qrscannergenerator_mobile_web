import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/qrgenerator_controller.dart';

import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrgeneratorView extends GetView<QrgeneratorController> {
  // late final qrFutureBuilder;
  // QrgeneratorView(){
  //   qrFutureBuilder = FutureBuilder<ui.Image>(
  //     //future: _loadOverlayImage(),
  //     future: Get.find<QrgeneratorController>().loadOverlayImage(),
  //     builder: (ctx, snapshot) {
  //       final size = 280.0;
  //       if (!snapshot.hasData) {
  //         return Container(width: size, height: size);
  //       }
  //       return CustomPaint(
  //         size: Size.square(size),
  //         painter: QrPainter(
  //           data: controller.message.toString(),
  //           version: QrVersions.auto,
  //           eyeStyle: const QrEyeStyle(
  //             eyeShape: QrEyeShape.square,
  //             color: Color(0xff128760),
  //           ),
  //           dataModuleStyle: const QrDataModuleStyle(
  //             dataModuleShape: QrDataModuleShape.circle,
  //             color: Color(0xff1a5441),
  //           ),
  //           // size: 320.0,
  //           // embeddedImage: snapshot.data,
  //           embeddedImageStyle: QrEmbeddedImageStyle(
  //             size: Size.square(60),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QrcodegeneratorView'),
        centerTitle: true,
      ),
      body:  SafeArea(
        top: true,
        bottom: true,
        child: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Container(
                    width: 280,
                    child: controller.qrFutureBuilder,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40)
                    .copyWith(bottom: 40),
                child: Text(controller.message.toString(),),
              ),
            ],
          ),
        ),
      ),
    );
  }


}
