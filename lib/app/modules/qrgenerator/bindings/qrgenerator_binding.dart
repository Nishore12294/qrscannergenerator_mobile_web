import 'package:get/get.dart';

import '../controllers/qrgenerator_controller.dart';

class QrgeneratorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<QrgeneratorController>(
      () => QrgeneratorController(),
    );
  }
}
