import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'dart:ui' as ui;

import 'package:qr_flutter/qr_flutter.dart';

class QrgeneratorController extends GetxController {
  //TODO: Implement QrgeneratorController
  late final qrFutureBuilder;
  final count = 0.obs;
  RxString message = "" .obs;
  @override
  void onInit() {
    super.onInit();
    message = "Nishorekumar -- 9994081073  \n nishorekumar@gmail.com !@#^&*(%^)" .obs;
  }


  QrgeneratorController(){
    qrFutureBuilder = FutureBuilder<ui.Image>(
      //future: Get.find<QrgeneratorController>().loadOverlayImage(),
      future:loadOverlayImage(),
      builder: (ctx, snapshot) {
        final size = 280.0;
        if (!snapshot.hasData) {
          return Container(width: size, height: size);
        }
        return CustomPaint(
          size: Size.square(size),
          painter: QrPainter(
            data: message.toString(),
            version: QrVersions.auto,
            eyeStyle: const QrEyeStyle(
              eyeShape: QrEyeShape.square,
              color: Color(0xff128760),
            ),
            dataModuleStyle: const QrDataModuleStyle(
              dataModuleShape: QrDataModuleShape.circle,
              color: Color(0xff1a5441),
            ),
            // size: 320.0,
            // embeddedImage: snapshot.data,
            embeddedImageStyle: QrEmbeddedImageStyle(
              size: Size.square(60),
            ),
          ),
        );
      },
    );
  }


  Future<ui.Image> loadOverlayImage() async {
    final completer = Completer<ui.Image>();
    final byteData = await rootBundle.load('assets/images/augusta_logo.png');
    ui.decodeImageFromList(byteData.buffer.asUint8List(), completer.complete);
    return completer.future;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
