
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrMobileScannerController extends GetxController {
  //TODO: Implement QrMobileScannerController

  final count = 0.obs;
  Barcode? result;
  QRViewController? qrViewController;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
   // super.reassemble();
    if (Platform.isAndroid) {
      qrViewController!.pauseCamera();
    }
    qrViewController!.resumeCamera();
  }



  @override
  void onInit() {
    super.onInit();
  }

  void updateWidget(){
     update(['qrMobileScannerId']);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    qrViewController?.dispose();
  }
  void increment() => count.value++;
}
