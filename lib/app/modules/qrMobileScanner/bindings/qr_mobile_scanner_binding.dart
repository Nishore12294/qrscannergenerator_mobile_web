import 'package:get/get.dart';

import '../controllers/qr_mobile_scanner_controller.dart';

class QrMobileScannerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<QrMobileScannerController>(
      () => QrMobileScannerController(),
    );
  }
}
