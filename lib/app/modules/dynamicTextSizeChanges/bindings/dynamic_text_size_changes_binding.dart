import 'package:get/get.dart';

import '../controllers/dynamic_text_size_changes_controller.dart';

class DynamicTextSizeChangesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DynamicTextSizeChangesController>(
      () => DynamicTextSizeChangesController(),
    );
  }
}
