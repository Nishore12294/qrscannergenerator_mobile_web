import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:get/get.dart';
import 'package:my_qrcode_getx/app/modules/dynamicTextSizeChanges/views/RestartWidget.dart';
import 'package:my_qrcode_getx/app/utilities/Constant.dart';

class DynamicTextSizeChangesController extends GetxController {
  //TODO: Implement DynamicTextSizeChangesController

  final count = 0.obs;
  //var valueHolder = 10.obs;
  RxInt valueHolder = 0 .obs;
  @override
  void onInit() {
    super.onInit();
    valueHolder = 50.obs;
  }

  void updateSlider(BuildContext context){
    print("valueHolder.value /100 :==>"+(valueHolder.value /10).toString());
    Constant.largeTextSize = Constant.fixedLargeTextSize + (valueHolder.value /10);
    Constant.mediumTextSize = Constant.fixedMediumTextSize + (valueHolder.value /10);
    Constant.smallTextSize = Constant.fixedSmallTextSize + (valueHolder.value /10);
    update(['sliderUpdationId']);
    //RestartWidget.restartApp(context);
    //Phoenix.rebirth(context);
    //Get. reset();

  }
  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
