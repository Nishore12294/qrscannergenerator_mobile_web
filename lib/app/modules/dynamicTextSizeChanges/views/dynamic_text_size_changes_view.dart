import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:my_qrcode_getx/app/modules/dynamicTextSizeChanges/views/RestartWidget.dart';
import 'package:my_qrcode_getx/app/utilities/Constant.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import '../controllers/dynamic_text_size_changes_controller.dart';

class DynamicTextSizeChangesView
    extends GetView<DynamicTextSizeChangesController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DynamicTextSizeChangesController>(
        id: 'sliderUpdationId',
        init: DynamicTextSizeChangesController(),
    builder: (value) =>  Scaffold(
        body: Center(
        child: Column(
            children: [
              Container(
                  margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                  child:Slider(
                      value: controller.valueHolder.toDouble(),
                      min: 1,
                      max: 100,
                      divisions: 100,
                      activeColor: Colors.green,
                      inactiveColor: Colors.grey,
                      label: '${ controller.valueHolder.round()}',
                      onChanged: (double newValue) {
                       // setState(() {
                        print("onChanged :==>"+newValue.toString());
                          controller.valueHolder.value = newValue.round();
                          controller.updateSlider(context);
                        print("onChanged final:==>"+controller.valueHolder.value.toString());
                        //});
                      },
                      semanticFormatterCallback: (double newValue) {
                        return '${newValue.round()}';
                      }
                  )),

              Text(controller.valueHolder.value.toString(), style: TextStyle(fontSize: 22),),

              Center(
                child: ElevatedButton(
                  onPressed: () {
                   //RestartWidget.restartApp(context);
                    print("Current Large Text Size :==>"+Constant.largeTextSize.toString());
                    print("Current Medium Text Size :==>"+Constant.mediumTextSize.toString());
                    print("Current Small Text Size :==>"+Constant.smallTextSize.toString());
                    Phoenix.rebirth(context);
                   // Get.reset();
                  },
                  child: const Text('Save'),
                ),
              ),
              Spacer(flex: 4,),
              Text('Large Text', style: TextStyle(fontSize: Constant.largeTextSize),),
              Spacer(flex: 4,),
              Text('Medium Text', style: TextStyle(fontSize: Constant.mediumTextSize),),
              Spacer(flex: 4,),
              Text('Small Text', style: TextStyle(fontSize: Constant.smallTextSize),)
            ]
        ))));
  }
}
