import 'package:get/get.dart';
import 'package:my_qrcode_getx/app/utilities/Constant.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    print("Current Large Text Size init:==>"+Constant.largeTextSize.toString());
    print("Current Medium Text Size init:==>"+Constant.mediumTextSize.toString());
    print("Current Small Text Size init :==>"+Constant.smallTextSize.toString());
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
