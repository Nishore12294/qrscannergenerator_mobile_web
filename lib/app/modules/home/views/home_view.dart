import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:my_qrcode_getx/app/modules/qrMobileScanner/views/qr_mobile_scanner_view.dart';
import 'package:my_qrcode_getx/app/modules/qrgenerator/views/qrgenerator_view.dart';
import 'package:my_qrcode_getx/app/routes/app_pages.dart';
import 'package:my_qrcode_getx/app/utilities/Constant.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Center(
              child: ElevatedButton(
                onPressed: () {
                  
                  //Get.to(QrgeneratorView());
                  Get.toNamed("/qrgenerator");

                },
                child: const Text('QR Generator'),
              ),
            ),

            Center(
              child: ElevatedButton(
                onPressed: () {
                  //Get.to(QrMobileScannerView());
                  Get.toNamed("/qr-mobile-scanner");
                },
                child: const Text('Mobile QR Scanner'),
              ),
            ),

            Center(
              child: ElevatedButton(
                onPressed: () {
                  //Get.to(QrMobileScannerView());
                  Get.toNamed("/dynamic-text-size-changes");
                },
                child: const Text('Dynamic Text Size changes'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
