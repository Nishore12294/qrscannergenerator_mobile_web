
class Constant{


  static double fixedLargeTextSize = 25;
  static double fixedMediumTextSize = 20;
  static double fixedSmallTextSize = 15;

  static double largeTextSize = 25;
  static double mediumTextSize = 20;
  static double smallTextSize = 15;

}