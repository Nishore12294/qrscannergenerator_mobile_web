part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const QRGENERATOR = _Paths.QRGENERATOR;
  static const QR_SCANNER = _Paths.QR_SCANNER;
  static const QR_MOBILE_SCANNER = _Paths.QR_MOBILE_SCANNER;
  static const DYNAMIC_TEXT_SIZE_CHANGES = _Paths.DYNAMIC_TEXT_SIZE_CHANGES;
}

abstract class _Paths {
  static const HOME = '/home';
  static const QRGENERATOR = '/qrgenerator'; // QR image generator
  static const QR_SCANNER = '/qr-scanner'; // Web scanner
  static const QR_MOBILE_SCANNER = '/qr-mobile-scanner'; // Mobile scanner
  static const DYNAMIC_TEXT_SIZE_CHANGES = '/dynamic-text-size-changes';
}
