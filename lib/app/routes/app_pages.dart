import 'package:get/get.dart';

import 'package:my_qrcode_getx/app/modules/dynamicTextSizeChanges/bindings/dynamic_text_size_changes_binding.dart';
import 'package:my_qrcode_getx/app/modules/dynamicTextSizeChanges/views/dynamic_text_size_changes_view.dart';
import 'package:my_qrcode_getx/app/modules/home/bindings/home_binding.dart';
import 'package:my_qrcode_getx/app/modules/home/views/home_view.dart';
import 'package:my_qrcode_getx/app/modules/qrMobileScanner/bindings/qr_mobile_scanner_binding.dart';
import 'package:my_qrcode_getx/app/modules/qrMobileScanner/views/qr_mobile_scanner_view.dart';
import 'package:my_qrcode_getx/app/modules/qrScanner/bindings/qr_scanner_binding.dart';
import 'package:my_qrcode_getx/app/modules/qrScanner/views/qr_scanner_view.dart';
import 'package:my_qrcode_getx/app/modules/qrgenerator/bindings/qrgenerator_binding.dart';
import 'package:my_qrcode_getx/app/modules/qrgenerator/views/qrgenerator_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.QRGENERATOR,
      page: () => QrgeneratorView(),
      binding: QrgeneratorBinding(),
    ),
    // GetPage(
    //   name: _Paths.QR_SCANNER,
    //   page: () => QrScannerView(),
    //   binding: QrScannerBinding(),
    // ),
    GetPage(
      name: _Paths.QR_MOBILE_SCANNER,
      page: () => QrMobileScannerView(),
      binding: QrMobileScannerBinding(),
    ),
    GetPage(
      name: _Paths.DYNAMIC_TEXT_SIZE_CHANGES,
      page: () => DynamicTextSizeChangesView(),
      binding: DynamicTextSizeChangesBinding(),
    ),
  ];
}
